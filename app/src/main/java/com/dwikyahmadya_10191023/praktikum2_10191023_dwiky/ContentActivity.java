package com.dwikyahmadya_10191023.praktikum2_10191023_dwiky;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class ContentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
    }
}